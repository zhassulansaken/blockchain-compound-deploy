const compound = new Compound(window.ethereum);

const ethStakeInput = document.getElementById('eth-stake');
const ethStakeButton = document.getElementById('eth-stake-button');
const ethUnstakeInput = document.getElementById('eth-unstake');
const ethUnstakeButton = document.getElementById('eth-unstake-button');
const enableEthereumButton = document.getElementById('enable-button');

enableEthereumButton.onclick = async () => {
  await ethereum.request({ method: 'eth_requestAccounts' });
};

ethStakeButton.onclick = async () => {
  const amount = +ethStakeInput.value;
  await supply(Compound.ETH, amount);
};

ethUnstakeButton.onclick = async () => {
  const amount = +ethUnstakeInput.value;
  await redeem(Compound.cETH, amount);
};

async function supply(asset, amount) {
  if (!isNaN(amount) && amount !== 0) {
    try {
      const trx = await compound.supply(asset, amount);
      console.log(asset, 'Stake', amount, trx);
      console.log('Transaction Hash', trx.hash);
    } catch (err) {
      alert(JSON.stringify(err));
    }
  }
}

async function redeem(asset, amount) {
  if (!isNaN(amount) && amount !== 0) {
    try {
      const trx = await compound.redeem(asset, amount);
      console.log(asset, 'Unstake', amount, trx);
      console.log('Transaction Hash', trx.hash);
    } catch (err) {
      alert(JSON.stringify(err));
    }
  }
}
